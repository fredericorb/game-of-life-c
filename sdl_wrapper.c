#include "sdl_wrapper.h"

static SDL_Rect black_sreen;

short init_game_window(const short width, const short height) {
	SDL_Init(SDL_INIT_VIDEO);
	int screen_width = (int) width*5;
	int screen_height = (int) height*5;

	_sdl_window = SDL_CreateWindow(
		"Game of Life",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		screen_width,
		screen_height,
		SDL_WINDOW_OPENGL
	);

	_sdl_renderer = SDL_CreateRenderer(_sdl_window, -1, SDL_RENDERER_ACCELERATED);

	if (_sdl_window == NULL) {
		SDL_Log("SDL Failed to open screen: %s\n", SDL_GetError());
		return SCREEN_FAIL;
	}

	_keyboard = malloc(sizeof(keymap_t));
	black_sreen.x = 0;
	black_sreen.y = 0;
	black_sreen.w = screen_width;
	black_sreen.h = screen_height;

	return NO_ERROR;
}

short draw(cell_t *c) {
	SDL_Rect r;
	r.x = c->x * 5;
	r.y = c->y * 5;
	r.w = 5;
	r.h = 5;

	SDL_SetRenderDrawColor(_sdl_renderer, 255, 0, 0, 255);
	SDL_RenderFillRect(_sdl_renderer, &r);
}

short render_next_frame() {
	SDL_RenderPresent(_sdl_renderer);
	SDL_Event e;		
	SDL_PollEvent(&e);
	if (e.type == SDL_QUIT) {
		_keyboard->quit = PRESSED;
	}
	SDL_Delay(40);
	SDL_SetRenderDrawColor(_sdl_renderer, 0, 0, 0, 255);
	SDL_RenderFillRect(_sdl_renderer, &black_sreen);
}

keymap_t* get_keyboard() {
	return _keyboard;
}

short close_game_window() {
	free(_keyboard);
	SDL_DestroyRenderer(_sdl_renderer);    
	SDL_DestroyWindow(_sdl_window);
	SDL_Quit();
}
