#include <stdio.h>
#include "sdl_wrapper.h"

#define TRUE 0
#define FALSE 1

#define WIDTH 100
#define HEIGHT 100

#define DEAD 0
#define ALIVE 1
#define DYING 2
#define RISING 3

static short game[WIDTH][HEIGHT];

void update_state();

int main() {
	short error = NULL;
	short quit  = FALSE;

	for (int i = 0; i < WIDTH; i++) {
		for (int j = 0; j < HEIGHT; j++) {
			game[i][j] = DEAD;
		}
	}

  // Draw initial state
	game[50][50] = ALIVE;
	game[49][50] = ALIVE;	
	game[51][50] = ALIVE;
	game[51][49] = ALIVE;
	game[50][48] = ALIVE;
	game[51][47] = ALIVE;
	game[49][47] = ALIVE;
	game[48][47] = ALIVE;
	game[46][47] = ALIVE;

	error = init_game_window(300, 200);
	if (error != NO_ERROR) {
		printf("Failed to open");
		return -1;
	}

	cell_t* c = malloc(sizeof(cell_t));
	while (quit != TRUE) {
		update_state();
		for (int i = 0; i < WIDTH; i++) {
			for (int j = 0; j < HEIGHT; j++) {
				if (game[i][j] == ALIVE) {
					c->x = i;
					c->y = j;
					draw(c);
				}
			}
		}
	
		render_next_frame();
		keymap_t* map = get_keyboard();
		if (map->quit == PRESSED) {
			printf("Quit pressed!\n");
			quit = TRUE;
		}
	}
}

void update_state() {
	for (int i = 0; i < WIDTH; i++) {
		for (int j = 0; j < HEIGHT; j++) {
			int living_around = 0;
			if (i > 0 && (game[i-1][j] == ALIVE || game[i-1][j] == DYING))
				living_around += 1;
			if (i > 0 && j > 0 && (game[i-1][j-1] == ALIVE || game[i-1][j-1] == DYING))
				living_around += 1;
			if (i > 0 && j < WIDTH && (game[i-1][j+1] == ALIVE || game[i-1][j+1] == DYING))
				living_around += 1;
			if (j > 0 && (game[i][j-1] == ALIVE || game[i][j-1] == DYING))
				living_around += 1;
			if (j < WIDTH && (game[i][j+1] == ALIVE || game[i][j+1] == DYING))
				living_around += 1;
			if (i < HEIGHT && (game[i+1][j] == ALIVE || game[i+1][j] == DYING))
				living_around += 1;
			if (i < HEIGHT && j > 0 && (game[i+1][j-1] == ALIVE || game[i+1][j-1] == DYING))
				living_around += 1;
			if (i < HEIGHT && j < WIDTH && (game[i+1][j+1] == ALIVE || game[i+1][j+1] == DYING))
				living_around += 1;

			if (game[i][j] == ALIVE && (living_around <= 1 || living_around >= 4)) {
				game[i][j] = DYING;
			}

			if (game[i][j] == DEAD && living_around == 3) 
				game[i][j] = RISING;
		}
	}

	for (int i = 0; i < WIDTH; i++) {
		for (int j = 0; j < HEIGHT; j++) {
			if (game[i][j] == DYING)
				game[i][j] = DEAD;
			else if (game[i][j] == RISING)
				game[i][j] = ALIVE;
		}
	}
}
