#ifndef GAME_OF_LIFE_C_SDL_WRAPPER_H
#define GAME_OF_LIFE_C_SDL_WRAPPER_H

#include <SDL2/SDL.h>

#define SHOULD_QUIT_TRUE 0
#define SHOULD_QUIT_FALSE 1

#define PRESSED 255
#define NOT_PRESSED 0

#define NO_ERROR NULL
#define SCREEN_FAIL 1 
#define WRONG_CELL 2

typedef struct {
	short x, y;
} cell_t;

typedef struct {
	short quit;
} keymap_t;

static SDL_Window* _sdl_window = NULL;
static SDL_Renderer* _sdl_renderer = NULL;
static keymap_t* _keyboard;
static short _sdl_should_close = SHOULD_QUIT_FALSE;

short init_game_window(short width, short height);

short draw(cell_t *c);

short render_next_frame();

short close_game_window();

keymap_t* get_keyboard(); 

#endif //GAME_OF_LIFE_C_SDL_WRAPPER_H
