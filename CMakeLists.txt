cmake_minimum_required(VERSION 3.12)
project(game_of_life_c C)

set(CMAKE_C_STANDARD 99)

add_executable(game_of_life_c main.c sdl_wrapper.h sdl_wrapper.c)

TARGET_LINK_LIBRARIES(game_of_life_c SDL2)
